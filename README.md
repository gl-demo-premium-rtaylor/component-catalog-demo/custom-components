# Custom CI/CD Components

This is an example project that demonstrates the basic structure and usage of GitLab CI/CD components.

## Usage

To use this component in a pipeline, add the following to your `.gitlab-ci.yml`:

```yaml
include: 
    - component: $CI_SERVER_FQDN/gl-demo-premium-rtaylor/component-catalog-demo/custom-components/build-container@$VERSION
      inputs:
        image: $BUILDER_IMAGE
        stage: build
```

## Testing

CI/CD components can be tested by creating a `.gitlab-ci.yml` in the component project's repository. In this example, we test to ensure that the `include` line properly adds the component to the pipeline.

## Releasing a new version

To release a new version of this component:
1. Create a new Git tag for the component using [Semantic Versioning](https://semver.org/) 
2. The test and release pipeline defined in `.gitlab-ci.yml` will run, and if all tests pass, the component will be published to the CI/CD Catalog.
